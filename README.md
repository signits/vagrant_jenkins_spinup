# Vagrant Jenkins Spinup

## Getting started

### The playbook installs and prepares virtualbox, vagrant, jenkins and molecule environments to run ansible-molecule tests and jenkins deployments automatically.

To run the playbook execute following:

```ansible-playbook vagrant_jenkins_vbox.yml -i host_ip_address, -K```

### The playbook was/is tested for Fedora 36 server VM installation (KVM/Virtualbox)
